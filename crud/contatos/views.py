from contatos.models import Contact
from contatos.serializers import ContactSerializer
from rest_framework import viewsets

# Create your views here.
class ContactView(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
