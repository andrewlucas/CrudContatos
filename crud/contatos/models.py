from django.db import models

# Create your models here.
class Contact(models.Model):
    name =  models.CharField(max_length=40)
    phone = models.IntegerField()
    email = models.CharField(max_length=60)
    CONTACTTYPE = (
        ('Técnico', 'Técnico'),
        ('Gestor', 'Gestor'),
    )
    contact_type = models.CharField(max_length=10, choices=CONTACTTYPE)
    priority = models.IntegerField()
